<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;;

class CastController extends Controller
{
    public function index()
    {
        $users = DB::table('cast')->get();
        return view('cast.index', compact('users'));
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => ['required', 'min:10', 'max:50'],
                'umur' => ['required', 'int'],
                'bio'  => ['required', 'min:10', 'max:50'],
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong!',
                'umur.required'  => 'Umur tidak boleh kosong!',
                'bio.required'  => 'Bio tidak boleh kosong!',
            ]
        );

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio'  => $request['bio'],
        ]);

        return redirect('/cast');
    }

    public function show($id)
    {
        $user = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('user'));
    }

    public function edit($id)
    {
        $user = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('user'));
    }

    public function update($id, Request $request)
    {
        $request->validate(
            [
                'nama' => ['required', 'min:10', 'max:50'],
                'umur' => ['required', 'int'],
                'bio'  => ['required', 'min:10', 'max:50'],
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong!',
                'umur.required'  => 'Umur tidak boleh kosong!',
                'bio.required'  => 'Bio tidak boleh kosong!',
            ]
        );

        DB::table('cast')
            ->where('id', $id)
            ->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]);

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
