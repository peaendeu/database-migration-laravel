@extends('layout.master')
@section('title', 'Edit Cast Page')
@section('content')
    <h1>Edit Cast</h1>

    <form action="/cast/{{ $user->id }}" method="post" class="col-md-4">
      @csrf
      @method('put')
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" autocomplete="off" autofocus  value="{{ $user->nama }}">
        @error('nama')
          <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">Umur</label>
        <input type="text" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" value="{{ $user->umur }}">
        @error('umur')
          <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control @error('bio') is-invalid @enderror" id="bio" name="bio" rows="3">{{ $user->bio }}</textarea>
        @error('bio')
          <small id="emailHelp" class="form-text text-danger">{{ $message }}</small>
        @enderror
      </div>
      <button type="submit" class="btn btn-dark">Submit</button>
    </form>
@endsection