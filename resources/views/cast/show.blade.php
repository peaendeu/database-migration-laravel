@extends('layout.master')
@section('title', 'Cast Detail')
@section('content')
  <h1>Cast Detail</h1>
  <div class="card mb-3 col-md-4 mt-3">
    <div class="row no-gutters">      
      <div class="col-md-8">
        <div class="card-body">
          <h5 class="card-title">{{ $user->nama }}</h5>
          <p class="card-text">{{ $user->umur }}</p>
          <p class="card-text"><small class="text-muted">{{ $user->bio }}</small></p>
          <a href="/cast" class="btn btn-dark">Back</a>
        </div>
      </div>
    </div>
  </div>   
@endsection