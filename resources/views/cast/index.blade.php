@extends('layout.master')
@section('title', 'Cast Page')    
@section('content')
    <h1>Cast</h1>
    <a href="/cast/create" class="btn btn-dark mb-3">Create New Cast</a>
    <table class="table col-md-8">
        <thead>
            <tr>
                <th scope="col">Number</th>
                <th scope="col">Name</th>                
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($users as $key => $user)
          <tr>
            <th scope="row">{{ $key+1 }}</th>
            <td>{{ $user->nama }}</td>
            <td>
              <form action="/cast/{{ $user->id }}" method="post">
                @csrf
                @method('delete')
                <a href="/cast/{{ $user->id }}" class="btn btn-outline-dark">Detail</a>
                <a href="/cast/{{ $user->id }}/edit" class="btn btn-outline-dark">Edit</a>
                <input type="submit" class="btn btn-outline-dark" value="Delete">
              </form>
            </td>
          </tr>   
          @empty
          <h3>No data, table is empty.</h3>
          @endforelse
        </tbody>
      </table>      
@endsection